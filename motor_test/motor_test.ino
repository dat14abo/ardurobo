static int h_mot_a = 8;
static int h_mot_b = 9;
static int v_mot_a = 6;
static int v_mot_b = 7;
static int v_mot_en = 5;
static int h_mot_en = 11;

void setup() {
    Serial.begin(57600);
  // put your setup code here, to run once:
  pinMode(h_mot_a, OUTPUT);
  pinMode(h_mot_b, OUTPUT);
  pinMode(v_mot_a, OUTPUT);
  pinMode(v_mot_b, OUTPUT);
  pinMode(h_mot_en,OUTPUT);
  pinMode(v_mot_en,OUTPUT);
  analogWrite(h_mot_en, 200); 
  analogWrite(v_mot_en, 200);

}

void read_pins(){
    Serial.println(digitalRead(h_mot_a));
    Serial.println(digitalRead(h_mot_b));
    Serial.println(digitalRead(v_mot_a));
    Serial.println(digitalRead(v_mot_b));
    
    }

void left_turn(){
  digitalWrite(h_mot_a, LOW);
  digitalWrite(h_mot_b,HIGH);
  digitalWrite(v_mot_b, LOW);
  digitalWrite(v_mot_a,HIGH);
  Serial.println("Left_turn");
  read_pins();
}


void right_turn(){
  digitalWrite(h_mot_a, HIGH);
  digitalWrite(h_mot_b,LOW);
  digitalWrite(v_mot_b, HIGH);
  digitalWrite(v_mot_a,LOW);
  Serial.println("Right_turn");
  read_pins();
}

void back(){
  digitalWrite(h_mot_b, HIGH);
  digitalWrite(h_mot_a,LOW);
  digitalWrite(v_mot_b, HIGH);
  digitalWrite(v_mot_a,LOW);
  Serial.println("Back");
  read_pins();
  
}

void forward(){
  digitalWrite(h_mot_a, HIGH);
  digitalWrite(h_mot_b,LOW);
  digitalWrite(v_mot_a, HIGH);
  digitalWrite(v_mot_b,LOW);  
  
  Serial.println("Forward");
  read_pins();
}

void stop_(){
  digitalWrite(h_mot_a, LOW);
  digitalWrite(h_mot_b,LOW);
  digitalWrite(v_mot_b, LOW);
  digitalWrite(v_mot_a,LOW); 
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("beginning");
  delay(500);
  forward();
  delay(500);
  right_turn();
  delay(500);
  left_turn();
  delay(500);
  back();
  delay(500);
  stop_();
  Serial.println("Done!");
}
