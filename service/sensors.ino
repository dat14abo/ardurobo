  /*Sensormetoder läggs här*/
 int ir_sense(){
  return analogRead(ir_fram);
}

/** 0 for OUTER LEFT, 1 for OUTER RIGHT, 2 for INNER LEFT, 3 for INNER RIGHT */
int line_sens(int side) {
  if(side == 0){
    return analogRead(linje_v); 
  }
  else if(side == 1) {
    return analogRead(linje_h);  
  }
  else if(side == 2) {
    return analogRead(linje_v_mid);
  }
  else if(side == 3) {
    return analogRead(linje_h_mid);
  }
  else{
    return -1;
  }
}

int ultra_sens(int side){
  if (side){
    digitalWrite(h_trig_ultra, LOW);
    delayMicroseconds(8);
    digitalWrite(h_trig_ultra, HIGH);
    delayMicroseconds(10);
    digitalWrite(h_trig_ultra, LOW);
    return pulseIn(h_echo_ultra, HIGH, 10000);
  } else {
    digitalWrite(v_trig_ultra, LOW);
    delayMicroseconds(8);
    digitalWrite(v_trig_ultra, HIGH);
    delayMicroseconds(10);
    digitalWrite(v_trig_ultra, LOW);
    return pulseIn(v_echo_ultra, HIGH, 10000);
  }
}

void read_sensor_data(){
  //sensor_data = {ir_fram, v_line, h_line, v_ultra, h_ultra}
  //Lämnar plats mellan läsningarna för att inte eko från den ena ska blöda in i nästa
  
  sensor_data[0]+=ir_sense();
  sensor_data[3]+=line_sens(0);
  sensor_data[4]+=line_sens(1);
  sensor_data[1]+=ultra_sens(0);
  sensor_data[2]+=ultra_sens(1);
  sensor_data[5]+=line_sens(2);
  sensor_data[6]+=line_sens(3);
}

int print_sensor_data(int avg, long time_for_loop){
  //Serial.println("ir_fram, v_line, h_line, v_ultra, h_ultra");
  Serial.print('s');//Qualifier for sensordata
  for(int i=0; i<7;i++){
	sensor_output[i]=sensor_data[i]/avg;
    //Serial.print("     ");
     
    Serial.print(sensor_output[i]);
    Serial.print(".");
    sensor_data[i]=0;
    }
	Serial.print(v_speed);
	Serial.print(".");
	Serial.print(h_speed);
	Serial.print(".");
	Serial.print(tt);
	
	Serial.println();
	
  return 0;
}
