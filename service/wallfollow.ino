void wall_follow(){
  int right;
  int left;
  int diff;
  int turn_right; // True = turn right, False = turn left

  right = sensor_output[1];
  left = sensor_output[0];
  turn_right = 0;

  if (right > left) {
    diff = right - left;
    turn_right = 1;
  }
  else {
    diff = left - right;
  }

  if (diff > 100 && diff < 1000 && turn_right)
  {
    v_speed += 5;
  }
  else if (diff > 100 && diff < 1000 && !turn_right)
  {
    h_speed += 5;
  }
  write_speed();
}