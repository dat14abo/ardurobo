void grapple(){
  // Turn on servo pins
  gripper.attach(klo_grip);
  lifter.attach(klo_lyft);

  int start_lift = 15;
  int stop_lift = 117;
  int start_grip = 80;
  int stop_grip = 115;
  int grip_lift_millis = 10;
  
  //Här passar koden för gripklon bra
   for (pos = start_grip; pos <= stop_grip; pos += 1) { 
    // in steps of 1 degree
    gripper.write(pos);
    ////read_print_data();              
    delay(grip_lift_millis);                       
  }
  
  for (pos = start_lift; pos <= stop_lift; pos += 1) { 
    // in steps of 1 degree
    lifter.write(pos);
    //read_print_data();              
    delay(grip_lift_millis);                       
  }
  
  for (pos = stop_grip; pos >= start_grip; pos -= 3) { 
    gripper.write(pos);
    //read_print_data();              
    delay(grip_lift_millis);                       
  }
  
  for (pos = stop_lift; pos >= start_lift; pos -= 3) {
    lifter.write(pos);
    //read_print_data();              
    delay(grip_lift_millis);                      
  }

  // Turn off servo pins
  gripper.detach();
  lifter.detach();
}

void set_grip_lift_pos(){
  gripper.attach(klo_grip);
  lifter.attach(klo_lyft);
  
  for(int i = 85; i > 80; i--){
    gripper.write(i);
    delay(100);
  }
  
  for(int i = 20; i > 15; i--){
    lifter.write(i);
    delay(100);
  }

  gripper.detach();
  lifter.detach();
}
