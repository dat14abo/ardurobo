/*All kod som har med rörel);break;}ra lägger vi här*/
//flagga för pausa exekvering.

void pause_exec() {
  stop_();
  pause = 1;
}
   
void resume_exec() {
  pause = 0;
}

/*Ofärdigt*/
void execute_forward(int row, int col, int dir) {
  forward();
}

void execute_turn(int dir) {
  /*Bara svänger, ingen reglering.*/
  sens_enable = 0;
  set_speed(turn_speed, 2);
  //Serial.println(turn_speed);
  
  if(dir) {
    right_turn();
  } else {
    left_turn();
  }

  Serial.println("Sleeping turntime");
  for(int i=0;i<turn_time;i++){
    delay(10);
    Serial.print(analogRead(linje_h_mid));
    Serial.print(" ");
    Serial.println(analogRead(linje_v_mid));
    
    }
  
  //delay(turn_time);
  stop_();
  pause = 1;
  sens_enable = 1;
}

void minimize_ultra(int dir){
	int h_ult=ultra_sens(1);//Store min
	int v_ult=ultra_sens(1);//Store min
	int at_index_h=0; 
	int at_index_v=0;
	int tmp_h,tmp_v;
	for(int i =0;i<100;i++){
		tmp_h=ultra_sens(1);
		tmp_v=ultra_sens(0);
		if (tmp_h<h_ult){
			h_ult=tmp_h;
			at_index_h=i;
		}
		if (tmp_v<v_ult){
			v_ult=tmp_v;
			at_index_v=i;
		}
		
}
}

void execute_foward_one_cell() {
  Serial.println("moving foward");
  //pause = 1;
  moving = 1;
  start_move = millis();
}

void perform_motion(int motion) {
/* right_turn=1, left_turn=0, foward_one_cell=2 */
if(motion==2) {
  execute_foward_one_cell();
  } else {
  execute_turn(motion);
  }
}

void write_speed() {
  //Serial.println("writing speed");
	analogWrite(h_mot_en, h_speed);
	analogWrite(v_mot_en, v_speed);
}

void adjust_speed(int side) {
	if(side) {
		h_speed--;
		v_speed++;
	} else {
		h_speed++;
		v_speed--;
	}
	write_speed();	
}

void set_speed(int hastighet, int side) { // 0 for left, 1 for right side, 2 for both
  if(hastighet < 0 && side) {
    hastighet = 30;
	  h_speed = 30;
  } else if(hastighet < 0 && !side) {
    hastighet = 30;
	  v_speed = 30;
  } else if(hastighet > 255 && side) {
    hastighet = 255;
    h_speed = 255;
  } else if(hastighet > 255 && !side) {
    hastighet = 255;
    v_speed = 255;
  }
  
  if(side == 1) {
    v_speed = hastighet;
    analogWrite(v_mot_en, hastighet);
  }
  else if(side == 0) {
    h_speed = hastighet;
    analogWrite(h_mot_en, hastighet);
  }
  else {
    v_speed = hastighet;
    h_speed = hastighet;
    write_speed();
  }
}

void forward(){
  digitalWrite(h_mot_a, LOW);
  digitalWrite(h_mot_b,HIGH);
  digitalWrite(v_mot_b, LOW);
  digitalWrite(v_mot_a,HIGH);
}


void back(){
  digitalWrite(h_mot_a, HIGH);
  digitalWrite(h_mot_b,LOW);
  digitalWrite(v_mot_b, HIGH);
  digitalWrite(v_mot_a,LOW);
}

void right_turn(){
  digitalWrite(h_mot_b, HIGH);
  digitalWrite(h_mot_a,LOW);
  digitalWrite(v_mot_b, HIGH);
  digitalWrite(v_mot_a,LOW);
}

void left_turn(){
  digitalWrite(h_mot_a, HIGH);
  digitalWrite(h_mot_b,LOW);
  digitalWrite(v_mot_a, HIGH);
  digitalWrite(v_mot_b,LOW);  
}

void stop_(){
  digitalWrite(h_mot_a, LOW);
  digitalWrite(h_mot_b,LOW);
  digitalWrite(v_mot_b, LOW);
  digitalWrite(v_mot_a,LOW);
}

// Styra manuellt med w/a/s/d/i/o/q
void test_manual_stearing() {
  char b = 'x';

  if(Serial.available()) {
    b = Serial.read();
    
    switch(b) {
    case 'w': {Serial.println("Foward");
        forward();break;}
            
    case 's': {Serial.println("back");
        back();break;}
            
    case 'a': {Serial.println("left");
        left_turn();break;}
            
    case 'd': {Serial.println("right");
        right_turn();break;}
            
    case 'q': {Serial.println("stop"); 
        stop_();break;}

    case 'i': {Serial.println("öka"); 
        v_speed+=10;
        h_speed+=10;
        set_speed(v_speed,2);break;}

    case 'o': {Serial.println("minska"); 
        v_speed-=10;
        h_speed-=10;
        set_speed(v_speed,2);break;}
    
    case 'g':{Serial.println("griper");
        grapple();}
    
    case 'h': {if(Serial.peek()>= '0' && Serial.peek()<='9'){
        h_speed=Serial.parseInt();}
        else {
        h_speed++;
        }
        write_speed(); break;}
                
    case 'j': {h_speed--;
        write_speed();break;}
    
    case 'v': {if(Serial.peek()>= '0' && Serial.peek()<='9'){
        v_speed=Serial.parseInt();}
        else {
        v_speed++;
        }
        
        write_speed();break;}
        
    case 'b': {v_speed--;
        write_speed();break;}
    
    case 'c': {if(pause){
        resume_exec();
        } else {
        pause_exec();}
        break;}

    case 'z':{execute_turn(0);break;}
    
    case 'x':{execute_turn(1);break;}
                
    }  
  } else {;//stop_();
  }
}
