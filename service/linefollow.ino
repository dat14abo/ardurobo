void line_follow(){
    int mid_right;
    int mid_left;
    int mid_diff;
    double diff_factor;
    int turn_right; // True = turn right, False = turn left

    mid_left = sensor_output[5];
    mid_right = sensor_output[6];
    diff_factor = 0.01;

    if (mid_right > mid_left) {
        mid_diff = (int) (diff_factor * (mid_right - mid_left) / 2);
        v_speed += mid_diff;
        h_speed -= mid_diff;
    } else if (mid_right <  mid_left) {
        mid_diff = (int) (diff_factor * (mid_left - mid_right) / 2);
        h_speed += mid_diff;
        v_speed -= mid_diff;
    }
    else if (mid_right < 300 && mid_left < 300)
    {
        // BOTH ARE OFF LINE ---> TURN 90 degrees
    }
    write_speed();
}