#include <Servo.h>

static int klo_grip = 12;
static int klo_lyft = 13;
static int ir_fram = A0;
static int linje_v = A1;
static int linje_h = A2;
static int linje_v_mid = A3;
static int linje_h_mid = A4;
static int h_trig_ultra = 4;
static int h_echo_ultra = 2;
static int v_trig_ultra = 10;
static int v_echo_ultra = 3;
static int h_mot_a = 8;
static int h_mot_b = 9;
static int v_mot_a = 6;
static int v_mot_b = 7;
static int v_mot_en = 11;
static int h_mot_en = 5;

int pos;

static uint8_t h_speed; // Right motor speed
static uint8_t v_speed; // Left motor speed
int moving = 0;         // Flag on if moving forward


static long time_,tt;
int pos;
static int avg;
long sensor_data[7];
int sensor_output[7];   // Calculated avg. from sensor data
int time_constant=30;   // Bestämmer antal samples på sensor medelvärde och även tidskonstanten i regulatorloopen
//int p_,i_;//PI-loop konstanter
long start_move=0;
int turn_time;          // Calculated time to turn at given turnspeed
int turn_speed;         // The speed at which to execute turn
bool sens_enable;       // Flag for sensors activity
int count;
int pause=0;            // För utvecklingssyfte

Servo gripper;
Servo lifter;

void set_speed(int hastighet, int side);
void right_turn();
void left_turn();

void back();
void execute_foward(int row, int col, int dir);
void execute_turn(int dir);
void stop_();
void write_speed();
void adjust_speed(int side);
void line_follow();

void setup() { 
  Serial.begin(57600);
  // Sensor setup
  pinMode(ir_fram,INPUT);
  pinMode(linje_h,INPUT);
  pinMode(linje_v,INPUT);
  pinMode(h_trig_ultra, OUTPUT);
  pinMode(h_echo_ultra, INPUT);
  pinMode(v_echo_ultra, INPUT);
  pinMode(v_trig_ultra, OUTPUT);
  pinMode(linje_h_mid, INPUT);
  pinMode(linje_v_mid, INPUT);
  // Motor setup
  pinMode(h_mot_a, OUTPUT);
  pinMode(h_mot_b, OUTPUT);
  pinMode(v_mot_a, OUTPUT);
  pinMode(v_mot_b, OUTPUT);
  pinMode(h_mot_en,OUTPUT);
  pinMode(v_mot_en,OUTPUT);
  
  v_speed = 75;
  h_speed = 82;
  count = 0;
  
  /*För att skapa medelvärde av sensordatan*/
  time_ = millis();
  avg = 0;

   /*
   * För att svänga.
   * tid det tar att svänga 90 grader i millis
   * hastighet i vilken att ta svängen
   * vi stänger av sensordata medan vi svänger för då har vi redan bestämt oss för att svänga
   */
  turn_time = 500; //millis
  turn_speed = 90; //40/255% max speed
  sens_enable = 1;
  write_speed();
  
  //set_grip_lift_pos();
  //delay(100);
  //Serial.println("Starting");
  forward();
  
  
  Serial.println("Turning");
  //c
  //execute_turn(0);
  //grapple();
  //execute_turn(1);
  forward();
}

int ultra_in_range() {
	if(sensor_output[1] > 50 && sensor_output[1] < 800) {
	// V_ULTRA SEES A WALL
		if(sensor_output[2] > 50 && sensor_output[2] < 800) {
		// Both see a wall
        // Serial.println("Ultra Report BOTH WALL");
		return 3;
		}
	// Only V_ULTRA
        Serial.println("Ultra Report V-WALL");
		return 2;
	} else if(sensor_output[2] > 50 && sensor_output[2] < 800) {
	// Only H_ULTRA
        Serial.println("Ultra Report H-WALL");
		return 1;
	} else {
	// NO Wall Closeby
        Serial.println("Ultra Report NO WALL");
		return 0;
	}
}

void regulate() {
	//Check sensor_output and adjust speed accordingly 
	if(ultra_in_range() == 3) {
		if(sensor_output[1] < sensor_output[2]) { //Vi är närmre ena väggen än andra
			count++;
			if(count > 12) {
			h_speed++;
			count = 0;
            }
        } else if(sensor_output[1] > sensor_output[2]) {
            int diff = sensor_output[1] - sensor_output[2];
            if(diff > 20) {
                ;
            } else {
                count++;
                if(count > 12) {
                    v_speed++;
                    count=0;    
                }
            }
        }
	}
<<<<<<< HEAD:service.ino
		Serial.println("WE ARE FUCKING HERE");
	if(sensor_output[4] > 750 && (ultra_in_range()!=3 && ultra_in_range()!=2)) {
        Serial.println("Found a line, right-hand");
        sens_enable=0;
	    stop_();
        execute_turn(1);    
	}else if(sensor_output[3] > 750 && (ultra_in_range()!=3 && ultra_in_range()!=1)  ) {
    Serial.println("Found a line, left-hand");
        sens_enable=0;
        stop_();
        execute_turn(0); 
    
	}else if((sensor_output[4] >750 || sensor_output[3] >750) && ultra_in_range()==0){
	    sens_enable=0;
        stop_();
        execute_turn(1); 
	    }
	if (sensor_output[5]<800){
		//left mid sens outside slight
		h_speed-=10;
	
	}else if (sensor_output[6]<800){
		v_speed-=10;
		}

	if(sensor_output[0] > 100 && sensor_output[0] < 180) {
	//sens_enable=0;
    ;//Serial.println("Time to grip");
	//stop_();
    //grapple();
    //sens_enable=1;
	}
 line_follow(); 
}


void loop() {
    if(pause) {
    Serial.println("------------------Exekvering pausad, 'c' for att starta------------------");
    sens_enable=0;
    }

    while(pause) {
        for(int i=0;i<100;i++) {
            if(i==99) {
                Serial.println("looping");
                delay(1000);
            }
        }
    
        if(Serial.available()) {
            char tmp=Serial.read();
            Serial.println(tmp);
            if(tmp == 'c') {
                Serial.println("Got 'c', Resume");
                sens_enable=1;
                resume_exec();
            } else {
                Serial.println("Only 'c' is accepted right now.");
            }
        }
    }
  
  
    if(sens_enable && ((millis() - time_) >= time_constant)) { // 80 millis för stunden
        avg = print_sensor_data(avg,tt);
        tt = millis() - time_;
        // Serial.println("Hello");
        time_ = millis();
        regulate();
    } else if(sens_enable) {
        read_sensor_data();
        regulate();
        avg++;
    }else{
        for(int i =0;i<7;i++){
            sensor_output[i]=0;
            }
        }
    test_manual_stearing();
    if(moving){
        forward();
    }
}
