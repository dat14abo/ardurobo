#!/usr/bin/python

"""
ZetCode PyQt5 tutorial

This example shows a QProgressBar widget.

Author: Jan Bodnar
Website: zetcode.com
"""

from PyQt5.QtWidgets import (QWidget, QProgressBar,
							 QPushButton, QApplication, QLabel)
from PyQt5.QtCore import (QBasicTimer, Qt)
import sys
import threading
from serial import Serial
import io

from queue import Queue
from PyQt5.QtWidgets import * 
from PyQt5 import QtCore, QtGui 
from PyQt5.QtGui import * 
from PyQt5.QtCore import * 



#Global serial

class KeyListener():
	def __init__(self,q):
		super().__init__()
		self.q=q;
	def on_key(self, key_recieved):
		#print(key_recieved);
		self.q.put(key_recieved)
		
		
		
class Example(QWidget):

	def __init__(self):
		super().__init__()
		super().setCursor(Qt.PointingHandCursor)
		self.initUI()
		#super().grabKeyboard()
			
	def initUI(self):
		'''
		self.terminal=QPlainTextEdit()
		self.terminal.insertPlainText("You can write text here.\n")
		self.terminal.move(200,200)
		self.terminal.resize(25,100)
		self.terminal.show()
		self.terminal.installEventFilter(self)
		'''
		
		self.sensors=[QProgressBar(self) for x in range(9)]
		self.names=["IR-FRAM", "V-ULTRA", "H-ULTRA", "V-LINJE", "H-LINJE", "H-LINJE-MID", "V-LINJE-MID", "V_SPEED", "H_SPEED"]
		[self.sensors[b].setToolTip(self.names[b]) for b in range(9)]
		[s.setMaximum(1000) for s in self.sensors]
		
		self.sensors[3].setGeometry(100, 50, 25, 50)
		self.sensors[3].setOrientation(Qt.Vertical)
		
		self.sensors[4].setGeometry(300, 50, 25, 50)
		self.sensors[4].setOrientation(Qt.Vertical)
		
		self.sensors[0].setMaximum(300)
		self.sensors[1].setMaximum(3000)
		self.sensors[2].setMaximum(3000)
		self.sensors[7].setMaximum(255)
		self.sensors[8].setMaximum(255)
		
		self.sensors[1].setGeometry(40, 10, 25, 80)
		self.sensors[2].setGeometry(320,10, 25, 80)
		self.sensors[2].setOrientation(Qt.Vertical)
		self.sensors[1].setOrientation(Qt.Vertical)
		#self.sensors[1].setValue(40)
		#self.sensors[2].setValue(40)
		
		self.sensors[0].setGeometry(200, 10, 25, 100)
		self.sensors[0].setOrientation(Qt.Vertical)
		self.sensors[0].setInvertedAppearance(True)
		
		self.sensors[7].setGeometry(30, 200, 100, 25)
		self.sensors[1].setTextVisible(False)
		#self.sensors[1].setInvertedAppearance(True)
		
		self.sensors[8].setGeometry(295, 200, 100, 25)
		self.sensors[2].setTextVisible(False)
		
		self.sensors[3].setGeometry(140, 50, 25, 50)
		self.sensors[3].setOrientation(Qt.Vertical)
		
		self.sensors[4].setGeometry(260, 50, 25, 50)
		self.sensors[4].setOrientation(Qt.Vertical)
		

		
		self.setGeometry(300, 300, 450, 250)
		self.setWindowTitle('SensorData')
		self.show()
		
	def eventFilter(self, QDialog, event):
		#print(event.type())
		if event.type() == QtCore.QEvent.KeyPress:
			print('YEY key')
			return True # means stop event propagation
		else:
			return QDialog.eventFilter(self, event)
			
			
	def on_data(self, e):
		i=0
		for a in e:
			self.sensors[i].setValue(float(a))
			i+=1

def read_serial_threaded(callback,q,ser):
	
	try:
		ser.open()
		ser.flush()
	except Exception as e:
		print(e)
		print("it's ok")

	print("Start ser")
	b=b'0'
	while True:
		if b != b'!\r\n':
			
			try:
				b=ser.readline()
				res=b.decode()
				if res[0]=='s':
					vals = b[1:].decode().split(".")
					#print(vals)
					callback(vals)
				else:
					if b:
						print(bytes(b))
					#print(b.decode())
				
			except Exception as e:
				print("in read serial: ", e)
				pass
		try:
			c=q.get(False)
			print('writing', c)
			ser.write(c[0].encode('utf8'))
			
		except Exception as e:
			print(e)
			#sys.exit(1)

			
			
def read_keyboard_threaded(callback):
	while True:
		callback(sys.stdin.read(1))
		
			
def main():
	
	app = QApplication(sys.argv)
	ex = Example()
	que=Queue()
	ser=""
	try:
		ser=Serial(baudrate=57600, port='COM3')
		#sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser),newline='\r')
	except Exception as e:
		print(e)
		
		#sys.exit(0)
	
	t2=threading.Thread(group=None, target=read_serial_threaded, args=(ex.on_data,que, ser ),daemon=True)
	key = KeyListener(que)
	t3=threading.Thread(group=None, target=read_keyboard_threaded, args=(key.on_key, ),daemon=True)
	
	t2.start()
	t3.start()
	res=app.exec_()
	sys.exit(res)


if __name__ == '__main__':
	main()