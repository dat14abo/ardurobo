static int ir_fram = A0;
static int linje_h = A1;
static int linje_v = A2;
static int h_trig_ultra = 4;
static int h_echo_ultra = 2;
static int v_trig_ultra = 10;
static int v_echo_ultra = 3;
int sensor_data[5];

void setup() {
    Serial.begin(57600);
    pinMode(ir_fram,INPUT);
  pinMode(linje_h,INPUT);
  pinMode(linje_v,INPUT);
    pinMode(h_trig_ultra, OUTPUT);
  pinMode(h_echo_ultra, INPUT);
  pinMode(v_echo_ultra, INPUT);
  pinMode(v_trig_ultra, OUTPUT);

}

  
 int ir_sense(){
  return analogRead(ir_fram);
  }

/** 0 for left, 1 for right */
int line_sens(int side){
  if(side == 0){
    return analogRead(linje_v); 
  }
  else if(side == 1){
    return analogRead(linje_h);  
  }
  else{
    return -1;
  }
}
int ultra_sens(int side){
  if (side){
  digitalWrite(h_trig_ultra, LOW);
  delayMicroseconds(8);
  digitalWrite(h_trig_ultra, HIGH);
  delayMicroseconds(10);
  digitalWrite(h_trig_ultra, LOW);
  return pulseIn(h_echo_ultra, HIGH, 5000);
  }else{
  digitalWrite(v_trig_ultra, LOW);
  delayMicroseconds(8);
  digitalWrite(v_trig_ultra, HIGH);
  delayMicroseconds(10);
  digitalWrite(v_trig_ultra, LOW);
  return pulseIn(v_echo_ultra, HIGH, 5000);
  }
}

void read_sensor_data(){
  //sensor_data = {ir_fram, v_line, h_line, v_ultra, h_ultra}
  //Lämnar plats mellan läsningarna för att inte eko från den ena ska blöda in i nästa
  
  
  sensor_data[0]=ir_sense();
  sensor_data[3]=line_sens(0);
  sensor_data[4]=line_sens(1);
  sensor_data[1]=ultra_sens(0);
  sensor_data[2]=ultra_sens(1);
    
  }

void print_sensor_data(int avg){
  //Serial.println("ir_fram, v_line, h_line, v_ultra, h_ultra");
  for(int i=0; i<5;i++){
    //Serial.print("     "); 
    Serial.print(sensor_data[i]/avg);
    Serial.print(".");
    }Serial.println();
  }
  
  


void loop() {
  // put your main code here, to run repeatedly:
    read_sensor_data();
    print_sensor_data(1);
    delay(100);
}
